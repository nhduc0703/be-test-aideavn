export enum SCHOOL_LEVEL {
  LV1 = 'LV1',
  LV2 = 'LV2',
  LV3 = 'LV3',
}

export enum TEST_LEVEL {
  ORAL_EXAM = 'ORAL_EXAM',
  FIFTEEN_MINUTEST_TEST = 'FIFTEEN_MINUTEST_TEST',
  ONE_HOUR_TEST = 'ONE_HOUR_TEST',
  SEMESTER_TEST = 'SEMESTER_TEST',
}
