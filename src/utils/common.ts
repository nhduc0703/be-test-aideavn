export function Padder(len?: number, pad?: string) {
  if (len === undefined) {
    len = 1;
  } else if (pad === undefined) {
    pad = '0';
  }

  let pads = '';
  while (pads.length < len) {
    pads += pad;
  }

  this.pad = function (what: string | number) {
    const s = what.toString();
    return pads.substring(0, pads.length - s.length) + s;
  };
}

export function b64Encode(value: string) {
  return Buffer.from(value, 'utf8').toString('base64');
}

export function b64Decode(value: string) {
  return Buffer.from(value, 'base64').toString('utf8');
}

export function mathRound(value: any, roundUp?: number) {
  if (!value) {
    return 0;
  }

  return parseFloat(
    parseFloat(value).toFixed(Number.isInteger(roundUp) ? roundUp : 0),
  );
}
