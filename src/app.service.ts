import { Injectable, Logger } from '@nestjs/common';
import { HealthCheckService, SequelizeHealthIndicator } from '@nestjs/terminus';

@Injectable()
export class AppService {
  constructor(
    private health: HealthCheckService,
    private db: SequelizeHealthIndicator,
  ) {}
  private readonly logger = new Logger(AppService.name);

  async getHello() {
    return 'Hello world!';
  }

  async healthCheck() {
    return this.health.check([() => this.db.pingCheck('database')]);
  }
}
