import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { QueryTypes } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';
import { Test } from 'src/database/models/test.model';
import { UpdateScoreDto } from './dto/update-score.dto';

@Injectable()
export class StudentsService {
  constructor(
    private readonly sequelize: Sequelize,

    @InjectModel(Test)
    private readonly test: typeof Test,
  ) {}

  async findOne(id: string) {
    const [student]: any = await this.sequelize.query(
      `
    select S.name                                                                     as "name",
        C.name                                                                        as "class",
        Sc.name                                                                       as "school",
        extract('year' from C."createdAt")                                            as "year",
        COALESCE((sum(T.score * T.coefficient)), 0) / COALESCE(sum(T.coefficient), 0) as "average"
    from "Students" S
            join "Classes" C on S."classId" = C."id"
            join "Schools" Sc on C."schoolId" = Sc.id
            join "Tests" T ON S.id = T."studentId"
    where S.id = :id
    group by S.name, C.name, Sc.name, C."createdAt"
    `,
      {
        replacements: { id },
        type: QueryTypes.SELECT,
      },
    );

    if (!student) {
      throw new BadRequestException('Student not found');
    }

    const subjects: any[] = await this.sequelize.query(
      `
        select S2.id,
            S2.name                         as "subject",
            COALESCE((sum(T.score * T.coefficient)), 0) /
            COALESCE(sum(T.coefficient), 0) as "average"
        from "Tests" T
            inner join "Subjects" S2 on T."subjectId" = S2.id
        where T."studentId" = '1cf99126-75d1-4b4f-86aa-575c27c6dca0'
        group by S2.id, S2.name`,
      {
        replacements: { id },
        type: QueryTypes.SELECT,
      },
    );

    for (const subject of subjects) {
      subject.tests = await this.test.findAll({
        where: { studentId: id, subjectId: subject.id },
        raw: true,
        attributes: ['id', 'score', 'level', 'coefficient'],
      });
    }

    student.subjects = subjects;

    return student;
  }

  async updateScore(input: UpdateScoreDto) {
    const test = await this.test.findByPk(input.testId);

    if (!test) {
      throw new BadRequestException('Test not found');
    }

    await test.update(input);
  }
}
