import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UpdateScoreDto } from './dto/update-score.dto';
import { StudentsService } from './students.service';

@Controller('students')
@ApiTags('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Get('/:id')
  findOne(@Param('id') id: string) {
    return this.studentsService.findOne(id);
  }

  @Post('/update-score')
  updateScore(@Body() input: UpdateScoreDto) {
    return this.studentsService.updateScore(input);
  }
}
