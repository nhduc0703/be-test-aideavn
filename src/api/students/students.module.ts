import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Test } from 'src/database/models/test.model';
import { StudentsController } from './students.controller';
import { StudentsService } from './students.service';

@Module({
  imports: [SequelizeModule.forFeature([Test])],
  controllers: [StudentsController],
  providers: [StudentsService],
})
export class StudentsModule {}
