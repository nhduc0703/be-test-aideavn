import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { TEST_LEVEL } from 'src/utils/enums';

export class UpdateScoreDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @IsUUID()
  readonly studentId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @IsUUID()
  readonly testId: string;

  @ApiPropertyOptional()
  @IsInt()
  @IsOptional()
  readonly coefficient?: number;

  @ApiPropertyOptional({ enum: TEST_LEVEL, enumName: 'TEST_LEVEL' })
  @IsEnum(TEST_LEVEL)
  @IsOptional()
  readonly level?: TEST_LEVEL;
}
