import { Controller, Get } from '@nestjs/common';
import { HealthCheck } from '@nestjs/terminus';
import { AppService } from './app.service';

@Controller('')
export class AppController {
  constructor(private appService: AppService) {}

  @Get('')
  getHello() {
    return this.appService.getHello();
  }

  @Get('health')
  @HealthCheck()
  healthCheck() {
    return this.appService.healthCheck();
  }
}
