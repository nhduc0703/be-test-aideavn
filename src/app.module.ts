import { Global, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule as AppConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { TerminusModule } from '@nestjs/terminus';
import configuration from 'src/config/configuration';
import { AppLoggerMiddleware } from 'src/middlewares/app-logger.middleware';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { Class } from './database/models/class.model';
import { School } from './database/models/school.model';
import { Student } from './database/models/student.model';
import { Subject } from './database/models/subject.model';
import { Teacher } from './database/models/teacher.model';
import { Test } from './database/models/test.model';
import { StudentsModule } from './api/students/students.module';

@Global()
@Module({
  imports: [
    AppConfigModule.forRoot({ load: [configuration], isGlobal: true }),
    DatabaseModule,
    TerminusModule,
    SequelizeModule.forFeature([
      Class,
      School,
      Student,
      Subject,
      Teacher,
      Test,
    ]),
    StudentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(AppLoggerMiddleware).forRoutes('*');
  }
}
