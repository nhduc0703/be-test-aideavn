import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { ApiPropertyEnum } from 'src/utils/api-property-enum';
import { SCHOOL_LEVEL } from 'src/utils/enums';
import { Teacher } from './teacher.model';

@Table
export class School extends Model<School> {
  @ApiProperty()
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    allowNull: false,
  })
  id: string;

  @ApiProperty()
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiPropertyEnum({ enum: SCHOOL_LEVEL, enumName: 'SCHOOL_LEVEL' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  level: SCHOOL_LEVEL;

  @ApiPropertyOptional()
  @Column({
    type: DataType.UUID,
  })
  principalId?: string;

  @ApiProperty()
  @CreatedAt
  createdAt: Date;

  @ApiProperty()
  @UpdatedAt
  updatedAt: Date;

  @ApiPropertyOptional()
  @BelongsTo(() => Teacher, 'principalId')
  principal?: string;
}
