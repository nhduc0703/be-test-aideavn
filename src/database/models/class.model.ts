import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { School } from './school.model';

@Table
export class Class extends Model<Class> {
  @ApiProperty()
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    allowNull: false,
  })
  id: string;

  @ApiProperty()
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiProperty()
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  @ForeignKey(() => School)
  schoolId: string;

  @ApiProperty()
  @CreatedAt
  createdAt: Date;

  @ApiProperty()
  @UpdatedAt
  updatedAt: Date;

  @ApiProperty()
  @BelongsTo(() => School)
  school: School;
}
