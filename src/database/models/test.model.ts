import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { ApiPropertyEnum } from 'src/utils/api-property-enum';
import { TEST_LEVEL } from 'src/utils/enums';
import { Student } from './student.model';
import { Subject } from './subject.model';

@Table
export class Test extends Model<Test> {
  @ApiProperty()
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    allowNull: false,
  })
  id: string;

  @ApiProperty()
  @Column({
    type: DataType.DOUBLE(),
    allowNull: false,
  })
  score: number;

  @ApiProperty()
  @Column({
    type: DataType.INTEGER(),
    allowNull: false,
  })
  coefficient: number;

  @ApiPropertyEnum({ enum: TEST_LEVEL, enumName: 'TEST_LEVEL' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  level: TEST_LEVEL;

  @ApiProperty()
  @Column({ type: DataType.UUID })
  @ForeignKey(() => Subject)
  subjectId: string;

  @ApiProperty()
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  @ForeignKey(() => Student)
  studentId: string;

  @ApiProperty()
  @CreatedAt
  createdAt: Date;

  @ApiProperty()
  @UpdatedAt
  updatedAt: Date;

  @ApiProperty()
  @BelongsTo(() => Subject)
  subject: Subject;

  @ApiProperty()
  @BelongsTo(() => Student)
  student: Student;
}
