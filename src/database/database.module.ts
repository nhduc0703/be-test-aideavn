import { Logger, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        const dbConfig = config.get('database');
        const logger = new Logger('SQL');

        return {
          ...dbConfig,
          autoLoadModels: true,
          logQueryParameters: true,
          synchronize: true,
          sync: { alter: { drop: false } },
          logging: dbConfig.logging
            ? (msg: string) => logger.debug(msg)
            : false,
        };
      },
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
