create database "be-test-aideavn";

create table "Schools"
(
    id            uuid                     not null
        constraint "Schools_pkey"
            primary key,
    name          varchar(255)             not null,
    level         varchar(255)             not null,
    "principalId" uuid,
    "createdAt"   timestamp with time zone not null,
    "updatedAt"   timestamp with time zone not null
);


create table "Classes"
(
    id          uuid                     not null
        constraint "Classes_pkey"
            primary key,
    name        varchar(255)             not null,
    "schoolId"  uuid                     not null
        constraint "Classes_schoolId_fkey"
            references "Schools"
            on update cascade,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null
);


create table "Students"
(
    id          uuid                     not null
        constraint "Students_pkey"
            primary key,
    name        varchar(255)             not null,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null,
    "classId"   uuid                     not null
        constraint "Students_classId_fkey"
            references "Classes"
            on update cascade
);


create table "Subjects"
(
    id           uuid                     not null
        constraint "Subjects_pkey"
            primary key,
    name         varchar(255)             not null,
    "createdAt"  timestamp with time zone not null,
    "updatedAt"  timestamp with time zone not null,
    "isRequired" boolean default false    not null
);


create table "Teachers"
(
    id          uuid                     not null
        constraint "Teachers_pkey"
            primary key,
    name        varchar(255)             not null,
    "schoolId"  uuid                     not null
        constraint "Teachers_schoolId_fkey"
            references "Schools"
            on update cascade,
    "subjectId" uuid
        constraint "Teachers_subjectId_fkey"
            references "Subjects"
            on update cascade,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null
);


alter table "Schools"
    add constraint "Schools_principalId_fkey"
        foreign key ("principalId") references "Teachers"
            on update cascade;

create table "Tests"
(
    id          uuid                     not null
        constraint "Tests_pkey"
            primary key,
    score       double precision         not null,
    level       varchar(255)             not null,
    "subjectId" uuid
        constraint "Tests_subjectId_fkey"
            references "Subjects"
            on update cascade,
    "studentId" uuid                     not null
        constraint "Tests_studentId_fkey"
            references "Students"
            on update cascade,
    "createdAt" timestamp with time zone not null,
    "updatedAt" timestamp with time zone not null,
    coefficient integer                  not null
);


