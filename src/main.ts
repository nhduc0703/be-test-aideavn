import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as compression from 'compression';
import { json, urlencoded } from 'express';
import { AppModule } from 'src/app.module';

declare const module: any;

async function bootstrap() {
  const logger = new Logger('Application', { timestamp: true });

  const app = await NestFactory.create(AppModule, { cors: true });
  const configService = app.get(ConfigService);

  const env = configService.get('env');
  const port = configService.get('port');
  const host = configService.get('host');

  app.setGlobalPrefix('api', { exclude: [''] });
  app.use(compression());
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.use(json({ limit: '50mb' }));

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
      whitelist: true,
      stopAtFirstError: true,
    }),
  );

  // documents
  const configDocument = new DocumentBuilder()
    .setTitle('BE TEST - AIDEAVN')
    .setDescription('')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, configDocument);
  SwaggerModule.setup('docs', app, document);

  await app.listen(port, host, async () => {
    logger.log(`is running on: ${await app.getUrl()} [${env}]`);
  });

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
